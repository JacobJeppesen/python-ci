Change Log
==========

All notable changes to ``app`` will be listed here.

[master] – YYYY-MM-DD
---------------------

Added
~~~~~

Changed
~~~~~~~

Deprecated
~~~~~~~~~~

Fixed
~~~~~

Development Changes
~~~~~~~~~~~~~~~~~~~


[0.10] – 2019-05-09
---------------------

Added
~~~~~
* Initial version

Changed
~~~~~~~
* Nothing

Fixed
~~~~~
* Nothing

Development Changes
~~~~~~~~~~~~~~~~~~~
* Nothing